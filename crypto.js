const DADOS_CRIPTOGRAFAR = {
    algoritmo : "aes256",
    segredo : "chaves",
    tipo : "hex"
};

const crypto = require("crypto");
const CryptoJS = require("crypto-js");

function criptografar(senha) {
    const cipher = crypto.Cipher(DADOS_CRIPTOGRAFAR.algoritmo, DADOS_CRIPTOGRAFAR.segredo);
    cipher.update(senha);
    return cipher.final(DADOS_CRIPTOGRAFAR.tipo);
};
function descriptografar(senha) {
    const decipher = crypto.Decipher(DADOS_CRIPTOGRAFAR.algoritmo, DADOS_CRIPTOGRAFAR.segredo);
    decipher.update(senha, DADOS_CRIPTOGRAFAR.tipo);
    return decipher.final('utf8');
};

function encrypt(textToCipher, algoritmo){
    const encrypted = CryptoJS[algoritmo].encrypt(textToCipher, DADOS_CRIPTOGRAFAR.segredo);
    return encrypted + "";
}
function decrypt(cipherText, algoritmo){
    const decrypted = CryptoJS[algoritmo].decrypt(cipherText, DADOS_CRIPTOGRAFAR.segredo);
    return decrypted.toString(CryptoJS.enc.Utf8);
}
function cryptoJSAES(textToCipher){
    const encrypted = CryptoJS.AES.encrypt(textToCipher, DADOS_CRIPTOGRAFAR.segredo);
    let decrypted = CryptoJS.AES.decrypt(encrypted + "", DADOS_CRIPTOGRAFAR.segredo);
    return {encrypted: encrypted + "", decrypted: decrypted.toString(CryptoJS.enc.Utf8)};
}

function hash(textToCipher, algoritmo){
    const encrypted = CryptoJS[algoritmo](textToCipher, DADOS_CRIPTOGRAFAR.segredo);
    return encrypted + "";
}

function cryptoJSSHA256(textToCipher){
    let encrypted = CryptoJS.SHA256(textToCipher, DADOS_CRIPTOGRAFAR.segredo);
    return encrypted + "";
}

console.log({
    DES: {
        encrypt: encrypt("Sou um texto criptografado com o Algoritmo DES", "DES"),
        decrypt: decrypt(encrypt("Sou um texto criptografado com o Algoritmo DES", "DES"), "DES")
    },
    AES: {
        encrypt: encrypt("Sou um texto criptografado com o Algoritmo AES", "AES"),
        decrypt: decrypt(encrypt("Sou um texto criptografado com o Algoritmo AES", "AES"), "AES"),
        v2: cryptoJSAES("Sou um texto criptografado com o Algoritmo AES")
    },
    MD5: {
        encrypt: hash("Sou um texto crifrado com o Algoritmo MD5", "MD5"),
    },
    SHA256: {
        encrypt: cryptoJSSHA256("Sou um texto crifrado com o Algoritmo SHA246")
    },
})